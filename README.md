Gnuplot-OCaml - Simple interface to Gnuplot
===========================================

---------------------------------------------------------------------------

**NOTE**: Simon Cruanes is the new maintainer of this project on [GitHub](https://github.com/c-cube/ocaml-gnuplot).
